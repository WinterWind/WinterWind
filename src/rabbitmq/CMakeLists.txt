set(LIBNAME winterwind-rabbitmq)

set(SRC_FILES ${SRC_FILES}
	channel.cpp
	connection.cpp
	envelope.cpp
	exchange.cpp
	inoutexchange.cpp
	log.cpp
	message.cpp
	queue.cpp)

set(HEADER_FILES ${HEADER_FILES}
	${INCLUDE_SRC_PATH}/rabbitmq/channel.h
	${INCLUDE_SRC_PATH}/rabbitmq/connection.h
	${INCLUDE_SRC_PATH}/rabbitmq/envelope.h
	${INCLUDE_SRC_PATH}/rabbitmq/exception.h
	${INCLUDE_SRC_PATH}/rabbitmq/exchange.h
	${INCLUDE_SRC_PATH}/rabbitmq/inoutexchange.h
	${INCLUDE_SRC_PATH}/rabbitmq/log.h
	${INCLUDE_SRC_PATH}/rabbitmq/message.h
	${INCLUDE_SRC_PATH}/rabbitmq/queue.h
	${INCLUDE_SRC_PATH}/rabbitmq/types.h)

set(PROJECT_LIBS ${PROJECT_LIBS} rabbitmq winterwind)

include_directories(${INCLUDE_SRC_PATH}/rabbitmq)

add_library(${LIBNAME} SHARED ${SRC_FILES})
target_link_libraries(${LIBNAME} ${PROJECT_LIBS})

install(FILES ${HEADER_FILES} DESTINATION ${INCLUDEDIR}/rabbitmq)

install(TARGETS ${LIBNAME}
	LIBRARY DESTINATION ${LIBDIR}
)
